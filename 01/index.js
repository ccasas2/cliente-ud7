class Poligono{
    constructor(puntos){
        this.puntos = puntos.sort()
    }
    iguales(p1,p2){
        return JSON.stringify(p1) === JSON.stringify(p2)
    }
    distancia(p1, p2){
        let distancia =  Math.sqrt((p2[0] - p1[0])**2 + (p2[1]- p1[1])**2)
        return distancia
    }
    numeroPuntos(){
        if (this.puntos.length != this.lados){
            throw "numero invalido de lado";
        }
    }

    coinciden(){
        let contador = 0
        for (let i = 0; i< this.puntos.length ; i++){
            contador ++
            for (let j =contador; j<this.puntos.length;j++){
                if (this.iguales(this.puntos[i], this.puntos[j])){
                    throw "Los puntos no pueden conincidir";
                }
            }
        }
    }
    enLineaX(){
        let aumento = new Set()
        this.puntos.forEach(element => {
            aumento.add(element[0] / element[1])
        });
        console.log(aumento)
        return aumento.size
    }
}

class Triangulo extends Poligono{
    lados = 3
    puntosValidos(){
        this.numeroPuntos()
        this.coinciden()
        if (this.enLineaX() === 1){
            throw "Los tres puntos estan en una misma linea";
        }
    }

    perimetro(){
        let total = 0
        total += this.distancia(this.puntos[0], this.puntos[1])
        total += this.distancia(this.puntos[0], this.puntos[2])
        total += this.distancia(this.puntos[1], this.puntos[2])
        return total
    }
}

class Cuadrado extends Poligono{
    lados = 4
    puntosValidos(){
        this.numeroPuntos()
        this.coinciden()
        ////faltaria comprobar que tiene 4 angulos rectos y 4 lados iguales

    }
    perimetro(){
        let distancia1 = this.distancia(this.puntos[0], this.puntos[1])
        let distancia2 = this.distancia(this.puntos[0], this.puntos[2])
        if (distancia1 <= distancia2){return distancia1*4}
        else {return distancia2*4}
    }
}

class rectangulo extends Poligono{
    lados = 4
    puntosValidos(){
        this.numeroPuntos()
        this.coinciden()
        //faltaria comprobar que tiene 4 angulos rectos y 2/2 lados

    }
    perimetro(){
        let distancias = Array()
        distancias.push(this.distancia(this.puntos[0], this.puntos[1]))
        distancias.push(this.distancia(this.puntos[0], this.puntos[2]))
        distancias.push(this.distancia(this.puntos[0], this.puntos[3]))
        distancias.sort()
        return (distancias[0]*2 + distancias[1]*2)
    }
}


let poli
//let puntos = Array([0,0],[2,0],[3,0])
//poli = new Triangulo(puntos)
//poli.puntosValidos()
//console.log(poli.perimetro())

 let puntos = Array([5,0],[0,5],[0,0],[5,5])
 poli= new rectangulo(puntos)
 console.log(poli.perimetro())